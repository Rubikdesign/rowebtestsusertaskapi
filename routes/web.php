<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\PagesController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get( 'tasks', [TaskController::class, 'ajaxTaskRequest']);
Route::post('tasks', [TaskController::class, 'ajaxRequestTaskCreate']);
Route::get('all', [TaskController::class, 'getAllTasksGroupedByWeek']);

// Route::get('/all', [PagesController::class , 'index']);
// Route::get('/getUsers', [PagesController::class , 'getUsers']);
// Route::post('/addUser', [PagesController::class , 'addUser']);
// Route::post('/updateUser', [PagesController::class , 'updateUser']);
// Route::get('/deleteUser/{id}', [PagesController::class , 'deleteUser']);

