@import url("https://fonts.googleapis.com/css?family=Nunito");
/*!
* Bootstrap v3.0.0
*
* Copyright 2013 Twitter, Inc
* Licensed under the Apache License v2.0
* http://www.apache.org/licenses/LICENSE-2.0
*
* Designed and built with all the love in the world by @mdo and @fat.
*/
/*! normalize.css v2.1.0 | MIT License | git.io/normalize */
article,
aside,
details,
figcaption,
figure,
footer,
header,
hgroup,
main,
nav,
section,
summary {
  display: block; }

audio,
canvas,
video {
  display: inline-block; }

audio:not([controls]) {
  display: none;
  height: 0; }

[hidden] {
  display: none; }

html {
  font-family: sans-serif;
  -webkit-text-size-adjust: 100%;
  -ms-text-size-adjust: 100%; }

body {
  margin: 0; }

a:focus {
  outline: thin dotted; }

a:active,
a:hover {
  outline: 0; }

h1 {
  font-size: 2em;
  margin: 0.67em 0; }

abbr[title] {
  border-bottom: 1px dotted; }

b,
strong {
  font-weight: bold; }

dfn {
  font-style: italic; }

hr {
  -moz-box-sizing: content-box;
  box-sizing: content-box;
  height: 0; }

mark {
  background: #ff0;
  color: #000; }

code,
kbd,
pre,
samp {
  font-family: monospace, serif;
  font-size: 1em; }

pre {
  white-space: pre-wrap; }

q {
  quotes: "\201C" "\201D" "\2018" "\2019"; }

small {
  font-size: 80%; }

sub,
sup {
  font-size: 75%;
  line-height: 0;
  position: relative;
  vertical-align: baseline; }

sup {
  top: -0.5em; }

sub {
  bottom: -0.25em; }

img {
  border: 0; }

svg:not(:root) {
  overflow: hidden; }

figure {
  margin: 0; }

fieldset {
  border: 1px solid #c0c0c0;
  margin: 0 2px;
  padding: 0.35em 0.625em 0.75em; }

legend {
  border: 0;
  padding: 0; }

button,
input,
select,
textarea {
  font-family: inherit;
  font-size: 100%;
  margin: 0; }

button,
input {
  line-height: normal; }

button,
select {
  text-transform: none; }

button,
html input[type="button"],
input[type="reset"],
input[type="submit"] {
  -webkit-appearance: button;
  cursor: pointer; }

button[disabled],
html input[disabled] {
  cursor: default; }

input[type="checkbox"],
input[type="radio"] {
  box-sizing: border-box;
  padding: 0; }

input[type="search"] {
  -webkit-appearance: textfield;
  -moz-box-sizing: content-box;
  -webkit-box-sizing: content-box;
  box-sizing: content-box; }

input[type="search"]::-webkit-search-cancel-button,
input[type="search"]::-webkit-search-decoration {
  -webkit-appearance: none; }

button::-moz-focus-inner,
input::-moz-focus-inner {
  border: 0;
  padding: 0; }

textarea {
  overflow: auto;
  vertical-align: top; }

table {
  border-collapse: collapse;
  border-spacing: 0; }

@media print {
  * {
    text-shadow: none !important;
    color: #000 !important;
    background: transparent !important;
    box-shadow: none !important; } }
*,
*:before,
*:after {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box; }

.container {
  margin-right: auto;
  margin-left: auto;
  padding-left: 15px;
  padding-right: 15px; }
  .container:before, .container:after {
    content: " ";
    /* 1 */
    display: table;
    /* 2 */ }
  .container:after {
    clear: both; }

.row {
  margin-left: -15px;
  margin-right: -15px; }
  .row:before, .row:after {
    content: " ";
    /* 1 */
    display: table;
    /* 2 */ }
  .row:after {
    clear: both; }

.col-xs-1,
.col-xs-2,
.col-xs-3,
.col-xs-4,
.col-xs-5,
.col-xs-6,
.col-xs-7,
.col-xs-8,
.col-xs-9,
.col-xs-10,
.col-xs-11,
.col-xs-12,
.col-sm-1,
.col-sm-2,
.col-sm-3,
.col-sm-4,
.col-sm-5,
.col-sm-6,
.col-sm-7,
.col-sm-8,
.col-sm-9,
.col-sm-10,
.col-sm-11,
.col-sm-12,
.col-md-1,
.col-md-2,
.col-md-3,
.col-md-4,
.col-md-5,
.col-md-6,
.col-md-7,
.col-md-8,
.col-md-9,
.col-md-10,
.col-md-11,
.col-md-12,
.col-lg-1,
.col-lg-2,
.col-lg-3,
.col-lg-4,
.col-lg-5,
.col-lg-6,
.col-lg-7,
.col-lg-8,
.col-lg-9,
.col-lg-10,
.col-lg-11,
.col-lg-12 {
  position: relative;
  min-height: 1px;
  padding-left: 15px;
  padding-right: 15px; }

.col-xs-1,
.col-xs-2,
.col-xs-3,
.col-xs-4,
.col-xs-5,
.col-xs-6,
.col-xs-7,
.col-xs-8,
.col-xs-9,
.col-xs-10,
.col-xs-11 {
  float: left; }

.col-xs-1 {
  width: 8.3333333333%; }

.col-xs-2 {
  width: 16.6666666667%; }

.col-xs-3 {
  width: 25%; }

.col-xs-4 {
  width: 33.3333333333%; }

.col-xs-5 {
  width: 41.6666666667%; }

.col-xs-6 {
  width: 50%; }

.col-xs-7 {
  width: 58.3333333333%; }

.col-xs-8 {
  width: 66.6666666667%; }

.col-xs-9 {
  width: 75%; }

.col-xs-10 {
  width: 83.3333333333%; }

.col-xs-11 {
  width: 91.6666666667%; }

.col-xs-12 {
  width: 100%; }

@media (min-width: 768px) {
  .container {
    max-width: 750px; }

  .col-sm-1,
  .col-sm-2,
  .col-sm-3,
  .col-sm-4,
  .col-sm-5,
  .col-sm-6,
  .col-sm-7,
  .col-sm-8,
  .col-sm-9,
  .col-sm-10,
  .col-sm-11 {
    float: left; }

  .col-sm-1 {
    width: 8.3333333333%; }

  .col-sm-2 {
    width: 16.6666666667%; }

  .col-sm-3 {
    width: 25%; }

  .col-sm-4 {
    width: 33.3333333333%; }

  .col-sm-5 {
    width: 41.6666666667%; }

  .col-sm-6 {
    width: 50%; }

  .col-sm-7 {
    width: 58.3333333333%; }

  .col-sm-8 {
    width: 66.6666666667%; }

  .col-sm-9 {
    width: 75%; }

  .col-sm-10 {
    width: 83.3333333333%; }

  .col-sm-11 {
    width: 91.6666666667%; }

  .col-sm-12 {
    width: 100%; }

  .col-sm-push-1 {
    left: 8.3333333333%; }

  .col-sm-push-2 {
    left: 16.6666666667%; }

  .col-sm-push-3 {
    left: 25%; }

  .col-sm-push-4 {
    left: 33.3333333333%; }

  .col-sm-push-5 {
    left: 41.6666666667%; }

  .col-sm-push-6 {
    left: 50%; }

  .col-sm-push-7 {
    left: 58.3333333333%; }

  .col-sm-push-8 {
    left: 66.6666666667%; }

  .col-sm-push-9 {
    left: 75%; }

  .col-sm-push-10 {
    left: 83.3333333333%; }

  .col-sm-push-11 {
    left: 91.6666666667%; }

  .col-sm-pull-1 {
    right: 8.3333333333%; }

  .col-sm-pull-2 {
    right: 16.6666666667%; }

  .col-sm-pull-3 {
    right: 25%; }

  .col-sm-pull-4 {
    right: 33.3333333333%; }

  .col-sm-pull-5 {
    right: 41.6666666667%; }

  .col-sm-pull-6 {
    right: 50%; }

  .col-sm-pull-7 {
    right: 58.3333333333%; }

  .col-sm-pull-8 {
    right: 66.6666666667%; }

  .col-sm-pull-9 {
    right: 75%; }

  .col-sm-pull-10 {
    right: 83.3333333333%; }

  .col-sm-pull-11 {
    right: 91.6666666667%; }

  .col-sm-offset-1 {
    margin-left: 8.3333333333%; }

  .col-sm-offset-2 {
    margin-left: 16.6666666667%; }

  .col-sm-offset-3 {
    margin-left: 25%; }

  .col-sm-offset-4 {
    margin-left: 33.3333333333%; }

  .col-sm-offset-5 {
    margin-left: 41.6666666667%; }

  .col-sm-offset-6 {
    margin-left: 50%; }

  .col-sm-offset-7 {
    margin-left: 58.3333333333%; }

  .col-sm-offset-8 {
    margin-left: 66.6666666667%; }

  .col-sm-offset-9 {
    margin-left: 75%; }

  .col-sm-offset-10 {
    margin-left: 83.3333333333%; }

  .col-sm-offset-11 {
    margin-left: 91.6666666667%; } }
@media (min-width: 992px) {
  .container {
    max-width: 970px; }

  .col-md-1,
  .col-md-2,
  .col-md-3,
  .col-md-4,
  .col-md-5,
  .col-md-6,
  .col-md-7,
  .col-md-8,
  .col-md-9,
  .col-md-10,
  .col-md-11 {
    float: left; }

  .col-md-1 {
    width: 8.3333333333%; }

  .col-md-2 {
    width: 16.6666666667%; }

  .col-md-3 {
    width: 25%; }

  .col-md-4 {
    width: 33.3333333333%; }

  .col-md-5 {
    width: 41.6666666667%; }

  .col-md-6 {
    width: 50%; }

  .col-md-7 {
    width: 58.3333333333%; }

  .col-md-8 {
    width: 66.6666666667%; }

  .col-md-9 {
    width: 75%; }

  .col-md-10 {
    width: 83.3333333333%; }

  .col-md-11 {
    width: 91.6666666667%; }

  .col-md-12 {
    width: 100%; }

  .col-md-push-0 {
    left: auto; }

  .col-md-push-1 {
    left: 8.3333333333%; }

  .col-md-push-2 {
    left: 16.6666666667%; }

  .col-md-push-3 {
    left: 25%; }

  .col-md-push-4 {
    left: 33.3333333333%; }

  .col-md-push-5 {
    left: 41.6666666667%; }

  .col-md-push-6 {
    left: 50%; }

  .col-md-push-7 {
    left: 58.3333333333%; }

  .col-md-push-8 {
    left: 66.6666666667%; }

  .col-md-push-9 {
    left: 75%; }

  .col-md-push-10 {
    left: 83.3333333333%; }

  .col-md-push-11 {
    left: 91.6666666667%; }

  .col-md-pull-0 {
    right: auto; }

  .col-md-pull-1 {
    right: 8.3333333333%; }

  .col-md-pull-2 {
    right: 16.6666666667%; }

  .col-md-pull-3 {
    right: 25%; }

  .col-md-pull-4 {
    right: 33.3333333333%; }

  .col-md-pull-5 {
    right: 41.6666666667%; }

  .col-md-pull-6 {
    right: 50%; }

  .col-md-pull-7 {
    right: 58.3333333333%; }

  .col-md-pull-8 {
    right: 66.6666666667%; }

  .col-md-pull-9 {
    right: 75%; }

  .col-md-pull-10 {
    right: 83.3333333333%; }

  .col-md-pull-11 {
    right: 91.6666666667%; }

  .col-md-offset-0 {
    margin-left: 0; }

  .col-md-offset-1 {
    margin-left: 8.3333333333%; }

  .col-md-offset-2 {
    margin-left: 16.6666666667%; }

  .col-md-offset-3 {
    margin-left: 25%; }

  .col-md-offset-4 {
    margin-left: 33.3333333333%; }

  .col-md-offset-5 {
    margin-left: 41.6666666667%; }

  .col-md-offset-6 {
    margin-left: 50%; }

  .col-md-offset-7 {
    margin-left: 58.3333333333%; }

  .col-md-offset-8 {
    margin-left: 66.6666666667%; }

  .col-md-offset-9 {
    margin-left: 75%; }

  .col-md-offset-10 {
    margin-left: 83.3333333333%; }

  .col-md-offset-11 {
    margin-left: 91.6666666667%; } }
@media (min-width: 1200px) {
  .container {
    max-width: 1170px; }

  .col-lg-1,
  .col-lg-2,
  .col-lg-3,
  .col-lg-4,
  .col-lg-5,
  .col-lg-6,
  .col-lg-7,
  .col-lg-8,
  .col-lg-9,
  .col-lg-10,
  .col-lg-11 {
    float: left; }

  .col-lg-1 {
    width: 8.3333333333%; }

  .col-lg-2 {
    width: 16.6666666667%; }

  .col-lg-3 {
    width: 25%; }

  .col-lg-4 {
    width: 33.3333333333%; }

  .col-lg-5 {
    width: 41.6666666667%; }

  .col-lg-6 {
    width: 50%; }

  .col-lg-7 {
    width: 58.3333333333%; }

  .col-lg-8 {
    width: 66.6666666667%; }

  .col-lg-9 {
    width: 75%; }

  .col-lg-10 {
    width: 83.3333333333%; }

  .col-lg-11 {
    width: 91.6666666667%; }

  .col-lg-12 {
    width: 100%; }

  .col-lg-push-0 {
    left: auto; }

  .col-lg-push-1 {
    left: 8.3333333333%; }

  .col-lg-push-2 {
    left: 16.6666666667%; }

  .col-lg-push-3 {
    left: 25%; }

  .col-lg-push-4 {
    left: 33.3333333333%; }

  .col-lg-push-5 {
    left: 41.6666666667%; }

  .col-lg-push-6 {
    left: 50%; }

  .col-lg-push-7 {
    left: 58.3333333333%; }

  .col-lg-push-8 {
    left: 66.6666666667%; }

  .col-lg-push-9 {
    left: 75%; }

  .col-lg-push-10 {
    left: 83.3333333333%; }

  .col-lg-push-11 {
    left: 91.6666666667%; }

  .col-lg-pull-0 {
    right: auto; }

  .col-lg-pull-1 {
    right: 8.3333333333%; }

  .col-lg-pull-2 {
    right: 16.6666666667%; }

  .col-lg-pull-3 {
    right: 25%; }

  .col-lg-pull-4 {
    right: 33.3333333333%; }

  .col-lg-pull-5 {
    right: 41.6666666667%; }

  .col-lg-pull-6 {
    right: 50%; }

  .col-lg-pull-7 {
    right: 58.3333333333%; }

  .col-lg-pull-8 {
    right: 66.6666666667%; }

  .col-lg-pull-9 {
    right: 75%; }

  .col-lg-pull-10 {
    right: 83.3333333333%; }

  .col-lg-pull-11 {
    right: 91.6666666667%; }

  .col-lg-offset-0 {
    margin-left: 0; }

  .col-lg-offset-1 {
    margin-left: 8.3333333333%; }

  .col-lg-offset-2 {
    margin-left: 16.6666666667%; }

  .col-lg-offset-3 {
    margin-left: 25%; }

  .col-lg-offset-4 {
    margin-left: 33.3333333333%; }

  .col-lg-offset-5 {
    margin-left: 41.6666666667%; }

  .col-lg-offset-6 {
    margin-left: 50%; }

  .col-lg-offset-7 {
    margin-left: 58.3333333333%; }

  .col-lg-offset-8 {
    margin-left: 66.6666666667%; }

  .col-lg-offset-9 {
    margin-left: 75%; }

  .col-lg-offset-10 {
    margin-left: 83.3333333333%; }

  .col-lg-offset-11 {
    margin-left: 91.6666666667%; } }
x
.form-group.error input[type=text] {
  border-color: red !important; }
x
.form-group.error:before {
  color: red; }

.input-hours {
  position: relative; }
  .input-hours input {
    padding-left: 40px !important; }
  .input-hours:before {
    content: "\f017";
    font-family: FontAwesome;
    font-style: normal;
    font-weight: normal;
    text-decoration: inherit;
    font-size: 18px;
    padding-right: 0.5em;
    position: absolute;
    top: 10px;
    left: 30px;
    color: #68ca39; }
  .input-hours.input-hours-error {
    color: red !important;
    border-color: red; }

input.input-text-normal {
  width: 100%;
  height: 40px;
  border: 1px solid #d4d7df;
  border-radius: 5px;
  padding: 0px 20px;
  color: #9BA0B9; }
  input.input-text-normal::-webkit-input-placeholder {
    /* Chrome/Opera/Safari */
    color: #dbdce6; }
  input.input-text-normal::-moz-placeholder {
    /* Firefox 19+ */
    color: #dbdce6; }
  input.input-text-normal:-ms-input-placeholder {
    /* IE 10+ */
    color: #dbdce6; }
  input.input-text-normal:-ms-input-placeholder {
    /* IE 10+ */
    color: #dbdce6; }
  input.input-text-normal:-moz-placeholder {
    /* Firefox 18- */
    color: #dbdce6; }
input.input-text-outline {
  width: 100%;
  height: 40px;
  border: 1px solid #68ca39;
  border-radius: 5px;
  padding: 0px 20px;
  color: #9BA0B9; }
  input.input-text-outline::-webkit-input-placeholder {
    /* Chrome/Opera/Safari */
    color: #68ca39; }
  input.input-text-outline::-moz-placeholder {
    /* Firefox 19+ */
    color: #68ca39; }
  input.input-text-outline:-ms-input-placeholder {
    /* IE 10+ */
    color: #68ca39; }
  input.input-text-outline:-moz-placeholder {
    /* Firefox 18- */
    color: #68ca39; }

.error .input-text-outline::-webkit-input-placeholder {
  /* Chrome/Opera/Safari */
  color: red; }
.error .input-text-outline::-moz-placeholder {
  /* Firefox 19+ */
  color: red; }
.error .input-text-outline:-ms-input-placeholder {
  /* IE 10+ */
  color: red; }
.error .input-text-outline:-moz-placeholder {
  /* Firefox 18- */
  color: red; }

.container-checkbox {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 14px;
  font-weight: normal;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  color: #9BA0B9; }

/* Create a custom checkbox */
.checkmark {
  position: absolute;
  top: 8px;
  left: 0;
  height: 20px;
  width: 20px;
  background-color: #fff;
  border: 1px solid #9BA0B9;
  border-radius: 15px;
  margin-right: 10px; }
  .checkmark.done {
    background-color: #3da9ff;
    border: none; }
    .checkmark.done:after {
      border: 1px solid #fff;
      border-top: none;
      border-right: none;
      content: "";
      height: 6px;
      left: 5px;
      position: absolute;
      top: 6px;
      transform: rotate(-45deg);
      width: 10px; }

/* Create the checkmark/indicator (hidden when not checked) */
.content-box {
  border: 1px solid #efefef;
  background: #fff;
  -webkit-box-shadow: 0px 0px 4px 1px #efefef;
  -moz-box-shadow: 0px 0px 4px 1px #efefef;
  box-shadow: 0px 0px 4px 1px #efefef;
  padding-bottom: 20px; }
  .content-box .errorTxt {
    border: 1px solid #dcc9ce;
    color: red;
    padding: 10px;
    display: none;
    background: #f1dbe0;
    margin-bottom: 10px;
    -webkit-box-shadow: 0px 0px 4px 1px #efefef;
    -moz-box-shadow: 0px 0px 4px 1px #efefef;
    box-shadow: 0px 0px 4px 1px #efefef; }
  .content-box .content-box-title {
    padding-bottom: 20px;
    overflow: hidden;
    padding-top: 20px; }
  .content-box .title {
    font-weight: bold;
    font-size: 24px;
    color: #1c2354; }
  .content-box .hour-count {
    font-weight: bold;
    color: #3e4974; }
  .content-box .hour-count span.fa {
    margin-left: 10px;
    color: #68ca39; }
  .content-box .button {
    border: none;
    outline: none;
    background: #3da9ff;
    color: #fff;
    padding: 10px 25px;
    border-radius: 5px; }
    .content-box .button:hover {
      background: #3899e6;
      cursor: pointer;
      text-decoration: none; }

.button-outline {
  border: none;
  outline: none;
  background: transparent;
  color: #3da9ff;
  padding: 10px 25px;
  border-radius: 5px;
  border: 1px solid #3da9ff;
  font-weight: bold; }
  .button-outline:hover {
    background: #3da9ff;
    cursor: pointer;
    color: #fff;
    text-decoration: none; }

.hours-count {
  color: #b3b7c8; }

.task-info {
  position: relative; }

.task-title-row {
  padding-left: 20px;
  color: #abafbc; }

/*# sourceMappingURL=taskApi.map */
