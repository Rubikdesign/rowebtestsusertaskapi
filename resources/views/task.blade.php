<!DOCTYPE html>
<html>
<head>
    @include('include.head')
</head>
<body style="background:#f9f9f9; padding: 50px 0px;">
<div class="container">
    <div class="col-md-6 col-md-offset-3 content-box">

        <div class="content-box-title">
            <span class="title">Tasks</span>
                @if(count($tasks) > 0)
                    <a href="{{ url('all') }}" class=" button-outline pull-right">Organize tasks</a>
                @endif
        </div>
        <table id="#userTable" class="table">
            <tbody id="tasks">
                @foreach ($tasks as $task)
                    <tr>
                        <td class="task-info">
                              <span class="checkmark @if($task-> done) done @endif"></span> <span class="task-title-row">{{ $task->name }}</span>
                         </td>
                        <td class="hour-count text-right">{{ $task->hours }} hours <span class="fa fa-clock-o"></span></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="errorTxt"></div>
        <div class="content-box-footer">
            <form action="" class="btn-submit row"  method="POST" name="addTask" id="addTask">
                <div class="form-group col-md-8">
                    <input type="text" name="name" class="input-text-normal" placeholder="Enter task here" required="">
                </div>
                <div class="form-group input-hours col-md-4">
                    <input type="text" name="hours" class="input-text-outline" placeholder="Enter hours..." required="">
                </div>
                <div class="form-group col-md-12">
                     <button class="button add-task" type="submit">Add task</button>
                </div>
            </form>
        </div>
    </div>
</div>
@include('include.footer')
<script type="text/javascript">
    jQuery.validator.setDefaults({
        debug: true,
        validClass: 'success',
        highlight: function (element, errorClass, validClass) {
        $(element).parents("div.form-group").addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function (element, errorClass, validClass) {
        $(element).parents(".error").removeClass(errorClass).addClass(validClass);
        }
    });
    $("#addTask").validate({
        rules: {
            name: 'required',
            hours: {
                required: true,
                number: true,
                max: 20,
                min: 0.5
            }
        },
    messages: {
        name : {
            required: "Please enter task name.",
        },
        hours: {
            required: "Please enter hours",
            number: "Please enter a valid number. ",
            max: "Please enter a value less than or equal to 20.",
            min : "Please enter a value greater than or equal to 0.5."
        },
    },
    errorElement: 'div',
    errorLabelContainer: '.errorTxt'
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(".add-task").click(function (e) {
        let IsValid = $("#addTask").valid();
        e.preventDefault();
        let title = $("input[name=name]").val();
        let details = $("input[name=hours]").val();
        let url = '{{ url('tasks') }}';
        if (IsValid) {
             $.ajax({
                url: url,
                method: 'POST',
                data: {
                    name: title,
                    hours: details
                },
                success: function (response) {
                    if (response.success) {
                        let tr_str = "<tr>" +
                        "<tr><td class='task-info'><span class='checkmark'></span> <span class='task-title-row'>" + title + "</span></td>" +
                        "<td class='hour-count text-right'> " + details + " hours<span class='fa fa-clock-o'></span></td>" +
                        "</tr>";
                        $("#tasks").append(tr_str);
                        let allTasks = " <a href='{{ url('all') }}' class=' button-outline pull-right'>Organize tasks</a>";
                        if (!$(".button-outline.pull-right")[0]){
                          $(".content-box-title").append(allTasks);
                        } 
                      } else {
                        console.log("Error");
                      }
                },
                error: function (error) {
                  console.log(error);
                }
            });
        }
    });
</script>
</body>
</html>