<!DOCTYPE html>
<html>
<head>
    @include('include.head')
</head>
<body style="background:#f9f9f9; padding: 50px 0px;">
<div class="container">
    <div class="col-md-6 col-md-offset-3 content-box">
        @foreach ($alltasks as $key => $value)
            <div class="task">
                @if($key != 0)
                    <div class="content-box-title">
                        <span class="title">Week {{$key+1}}</span>
                        <div class="hours-count"></div>
                    </div>
                @else
                    <div class="content-box-title">
                        <span class="title">Week 1</span>
                        <a href="{{ url('tasks') }}" class=" button-outline pull-right"> <span  class="fa fa-arrow-left "></span> Go Back</a>
                        <div class="hours-count"></div>
                    </div>
                @endif
                <table id="#userTable" class="table">
                    <tbody id="tasks">
                    @foreach ($value as $k => $v)
                        <tr>
                            <td class="task-info">
                                <span class="checkmark  @if($v['done']) done @endif""></span> <span class="task-title-row">{{ $v['name'] }}</span>
                            </td>
                            <td class="hour-count text-right"><span class="counter">{{ $v['hours'] }}</span> hours <span class="fa fa-clock-o"></span></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endforeach
    </div>
</div>
@include('include.footer')
<script>
$(document).ready(function () {
    $('div.task').each(function () {
        let sum = 0;
        $(this).find('.counter').each(function () {
            sum += Number($(this).text());
        });
        $(this).find('.hours-count').text(sum + ' - Hours');
    });
});
</script>
</body>
</html> 