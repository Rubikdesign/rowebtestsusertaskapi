##Laravel API -> RoWeb test
Version 1.0
Author: Adrian Costache
## How do I get set up?
Install Composer (See https://getcomposer.org/doc/00-intro.md) (for Mac OS X) sh git clone https://Rubikdesign@bitbucket.org/Rubikdesign/rowebtestsusertaskapi.git


cd to the document root of the rowebtestsusertaskapi After installation, open the hidden file .env (which is located in the document root) in any text editor of your choice Setup the db section like this:
- DB_CONNECTION=mysql
* DB_HOST=localhost;unix_socket=/Applications/MAMP/tmp/mysql/mysql.sock
* DB_PORT=3306
* DB_DATABASE={database_name}
* DB_USERNAME={user_name}
* DB_PASSWORD={password}


Test API
*  php artisan key:generate
*  php artisan migrate
*  php artisan serve

#Routes
## For example after (php artisan serve) server link will be http://127.0.0.1:8000:
* {serverLink}/api/tasks
* {serverLink}/api/all


