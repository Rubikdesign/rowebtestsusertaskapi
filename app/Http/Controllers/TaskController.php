<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class TaskController extends Controller
{
/**
* Display a listing of the resource.
*
* @return mixed
*/
public function ajaxTaskRequest()
{
    $tasks = Task::getUserTasks();
    return \View::make('task', array('tasks' => $tasks));
}

/**
* Store a newly created resource in storage.
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\JsonResponse
*/
public function ajaxRequestTaskCreate(Request $request)
{
    \DB::table('tasks')->insert([
'name'  => $request->name, //This coming from ajax request
'hours' => $request->hours, //This coming from ajax request
'done' => FALSE
]);
    return response()->json(
        [
            'success' => true,
            'message' => 'Task inserted successfully!'
        ]
    );
}

public function getAllTasksGroupedByWeek () {

    $allTasks = Task::getTasksGrouped();
    return \View::make('all', array('alltasks' => $allTasks));

}
}
