<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Task extends Model {
    use HasFactory;
/**
* The attributes that are mass assignable.
*
* @var array
*/
protected $fillable = [
    'name',
    'done',
    'hours',
    'id',
];
protected static $iMaxCount = 40;

/**
* @return \Illuminate\Support\Collection
*/
public static function getUserTasks() {

    $oTasks = DB::table('tasks')->get();
    return $oTasks;
}

/**
* @param $data
*
* @return array
*/
private static function prepareDataTogroup ( $data ) {

    $aResult = [0 => []];
    foreach ( $data as $i => $iValue ) {
        $row = $iValue;
        $currentResultKey = max(array_keys($aResult));
        $currentSum = 0;
        foreach ( $aResult[$currentResultKey] as $alreadyAdded ) {
            $currentSum += $alreadyAdded['hours'];
        }
        if ( $currentSum + $row['hours'] <= static::$iMaxCount ) {
            $aResult[$currentResultKey][] = $row;
        } else {
            $aResult[$currentResultKey + 1] = [];
        }
    }
    return $aResult ;
}

/**
* @return array
*/
public static function getTasksGrouped() : array {
    $aTasks = self::orderBy('hours')->get()->all();
    $aResult = self::prepareDataTogroup( $aTasks );
    return $aResult ?? [];
}
}
